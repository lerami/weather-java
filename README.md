## Weather-Java

Weather-Java is an application that allows to automatically collect some weather forecast from the web, with location specified. The information is displayed in the console.

This application is implemented in Java, with Eclipse.

#### Installation Libraries

You will need to get those libraries in order to run the application:

[Import org.json](http://www.java2s.com/Code/Jar/o/Downloadorgjsonjar.htm)

[Import org.apache.commons.cli](http://www.java2s.com/Code/Jar/c/Downloadcommonscli20jar.htm)

[For having DefaultParse()](http://book2s.com/java/jar/c/commons-cli/download-commons-cli-1.3.1.jar.html) , if you don't have it.

In the project, those libraries are in the source folder *jar*.

#### API References

First, 3 APIs have been selected to collect information:

- [MetaWeather](https://www.metaweather.com/api/)


- [Prevision-meteo.ch](https://www.prevision-meteo.ch/services)


- [Yahoo! Weather](https://developer.yahoo.com/weather/)


Each API gives us weather forecast information of the specified location. In *src* source folder, the Java Package **weather_suppliers** contains Java Class for each API.


On each Java Class, information are collected with an HTTP GET request and are given as a JSON file. Therefore, object `JSONObject` is used to have all the information given by the API, and to parse it. 

But each API returns different response, that's why a Class has been created for each one. Though, an interface `InterfaceMeteo` stands as a wrapper for these APIs Class. 

#### Using the Application

This application can be runned directly from the shell, by entering this command line :

```java
java -jar weather.jar <location> -j <nb_days>
```

> `weather.jar` is an archive file we've made and an executable file as well

> `app.weather.ClientRequest` is the Main Class

This command line has to be written, so that the application will run correctly.

Alternatively, you can specify many other information you want by adding other flags. Below you can find several flags you can mention to have more details in weather forecast.

- Add humidity information with `-h`

```java
weather.jar <location> -j <nb_days> -h
```

- Add speed of wind with `-w`

```java
weather.jar <location> -j <nb_days> -w
```

- Specify type of temperature `-m` then choose if you want in Fahrenheit `F` or in Celsius `C`

```java
weather.jar <location> -j <nb_days> -m <F/C>
```

- Allow to display the result also in the mentioned file. The content of the former file is erased before adding the display result.

```java
weather.jar <location> -j <nb_days> -o <output_file_name>
```

- Allow to display the result also in the mentioned file. The content of the former file is kept, then followed by the display result.

```java
weather.jar <location> -j <nb_days> -a <output_file_name>
```



It has to be mentioned that all of these flags are treated with Java Class `Options` and `Option`. For more information about this, you can see [how to use Options](https://commons.apache.org/proper/commons-cli/usage.html).

Furthermore, Java Class `CommandLineParser` parses the written command line to check if options set in _true_ mark are mentioned. If not, Java Class `HelpFormatter` prints the command line user has to write in the command line interface, and puts all the possible flags in the list above. This handles errors from the command line.



#### Query History

The application maintains a query history file of all requests made.

> "requetes.log" is our query history file name.

The information of a request contains the date and hour of request, the HTTP response code returned by the API, and finally the URL where the HTTP GET is performed.

In order to get the date and hour, we use the Java Class `Calendar` and its method `getInstance()` to specify what data we want.

The HTTP response code returned is collected with Java Class`HttpURLConnection` and its method `getResponseCode`. We check if the code is valid by comparing it with field `HttpURLConnection.HTTP_OK`.

Those requests are added in our query history file by using Java Class `PrintStream` and `FileOutputStream`.  These two Class allows to redirect the output display into the specified file, in the following of what was written before.

Eventually, we redirect the output to default by closing `PrintStream` named _prStrmRequest_ and reset the standard output with _stdout_.

```java
prStrmRequest.close(); //close redirection for requests
System.setOut(stdout); //reset to standard output
```



#### Save File

Saving our information board display is the same as our query history, except that we use one more Java Class `TeeOutputStream`. This Class allows to redirect the output not only into our save file but even into the console.

At the end, file and redirections are closed.




#### The Board display

At first, we needed to create a class that can encompass all our `Weather` objects (each one representing one of our supplier's report). Choice has been made to create a class `WeatherSet` which is described by a `HashSet<Weather>` attribute and two methods : `addWeather` and `display`.

Displaying the informations has been done by creating several objects (`Cell`, `Line`, `Table`). The weather report is printing as a `String` object when filling a new `Weather` object. This `String` is then parsed using the `Pattern` object to retrieve the informations of the supplier's name and the temperature, wind speed and humidity of each day.

Finally, the board can be displayed by calling the `display` method on the `WeatherSet` object.




#### Icons for the weather conditions (BONUS)

To finish this project, we've added a bunch of icons to image the weather condition (if it's cloudy, rainy, sunny, slightly cloudy or thunder). To do so, we've created a new class `Icon` in the *display* package. Moreover, we've added some static values in the `InterfaceMeteo` wrapper to homogenize the weather conditions results obtained through the APIs.  

