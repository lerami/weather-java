package weather;

//import java.io.BufferedWriter;
//import java.io.File;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.util.regex.Pattern;

import org.apache.commons.cli.*;

import suppliers.*;

import java.io.*;

//import org.apache.commons.cli.*;
import org.apache.commons.io.output.TeeOutputStream;

public class ClientRequest {

	public static void main(String[] args) throws Exception {

		if (args.length == 0)
			System.err.println("Error : Please write at least the name of town you want.");

		// Creating all the options that could be pass as an arguments of the
		// command line
		Options options = new Options();

		Option days = new Option("j", "number-of-days", true, "number of days for the forecast");
		days.setRequired(true); // obligatory
		options.addOption(days);

		Option output = new Option("o", "output", true, "save result in a file by deleting its initial content");
		options.addOption(output);

		Option safe_output = new Option("a", "safe-output", true,
				"save result in a file without deleting its content.");
		options.addOption(safe_output);

		Option humidity = new Option("h", "humidity", false, "add humidity to the results");
		options.addOption(humidity);

		Option metric = new Option("m", "metric-unit", true, "choose between C for Celsius or F for Farenheit");
		options.addOption(metric);

		Option wind = new Option("w", "wind-speed", false, "add wind speed to the results");
		options.addOption(wind);

		Option farhenheit = new Option("f", "farhenheit", false, "convert temperature into farhenheit");
		options.addOption(farhenheit);

		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		CommandLine cmd;

		try {
			// parse the command line arguments
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			// something went wrong during the parse.
			System.out.println(e.getMessage());
			formatter.printHelp("utility-name", options); // automatically
															// generate the help
															// statement

			System.exit(1);
			return;
		}

		// Start of the application
		boolean h = false;
		boolean w = false;
		boolean f = false;

		// Retrieving the number of day
		String days_opt = cmd.getOptionValue("number-of-days");
		int nb_days = Integer.parseInt(days_opt);

		// Getting the city name
		String city = args[0];

		// Getting the boolean f to know if temperature should be printed in
		// Celsius or Farhenheit degrees
		if (cmd.hasOption("metric-unit")) {
			String metric_opt = cmd.getOptionValue("metric-unit");
			if (metric_opt.equals("F"))
				f = true;
			if (metric_opt.equals("C"))
				f = false;
		}

		// Getting the values
		if (cmd.hasOption("humidity")) {
			h = true;
		}

		if (cmd.hasOption("wind-speed")) {
			w = true;
		}

		// if (cmd.hasOption("farhenheit")) {
		// f = true;
		// }

		// Preparing the application for the saving of request on the file
		// requetes.log
		PrintStream stdout = System.out;
		PrintStream prStrmRequest = null;
		boolean req = false;
		// open or create the "requetes.log" file.
		try {
			FileOutputStream fileSave = new FileOutputStream("requetes.log", true);
			// TeeOutputStream myOut=new TeeOutputStream(System.out, fileSave);
			prStrmRequest = new PrintStream(fileSave, true);
			System.setOut(prStrmRequest);
			req = true;
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Calling our APIs (writing of the request on the saving file)
		APIPrevisionMeteoCh api1 = new APIPrevisionMeteoCh();
		APIMetaWeather api2 = new APIMetaWeather();
		APIYahooWeather api3 = new APIYahooWeather();

		WeatherSet weather = new WeatherSet();

		weather.addWeather(api1.weatherReport(city, nb_days));
		weather.addWeather(api2.weatherReport(city, nb_days));
		weather.addWeather(api3.weatherReport(city, nb_days));

		if (req) {
			prStrmRequest.close(); // close redirection for requests
			System.setOut(stdout); // reset to standard output
		}

		PrintStream prStrm = null;
		FileOutputStream fileSave = null;
		boolean saving = false;

		if (cmd.hasOption("output")) {
			// save in a file after delete.
			String outputPath = cmd.getOptionValue("output");

			try {
				// open or create the file with the path specified: to change if
				// necessary;
				fileSave = new FileOutputStream(outputPath, false);
				TeeOutputStream myOut = new TeeOutputStream(System.out, fileSave);
				prStrm = new PrintStream(myOut, true);
				System.setOut(prStrm);
				saving = true;
				// Every System Output will be copy to the mentioned file.
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (cmd.hasOption("safe-output")) {
			// save in a file without deleting
			String outputPath = cmd.getOptionValue("safe-output");

			try {
				fileSave = new FileOutputStream(outputPath, true);
				TeeOutputStream myOut = new TeeOutputStream(System.out, fileSave);
				prStrm = new PrintStream(myOut, true);
				System.setOut(prStrm);
				saving = true;
				// Every System Output will be copy to the mentioned file.
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// Display of the weather report
		weather.display(nb_days, h, w, f);

		if (saving) {
			fileSave.close();// prStrm.close();
			// here you can print something in the console but not in the file.

			prStrm.flush();
			prStrm.close();
			// Final.
		}

	}
}