package weather;

public interface InterfaceMeteo {

	public static int SUN = 0;
	public static int CLOUD = 1;
	public static int RAIN = 2;
	public static int THUNDER = 3;
	public static int LIGHT = 4;

	public Weather weatherReport(String city, int nb_days) throws Exception;

}
