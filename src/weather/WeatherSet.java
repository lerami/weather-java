package weather;

import java.util.HashSet;
import java.util.Set;

import display.Table;

public class WeatherSet {
	private Set<Weather> weathers;

	public WeatherSet() {
		weathers = new HashSet<Weather>();
	}

	public void addWeather(Weather weather) {
		weathers.add(weather);
	}

	// This function is called in the Main Class to display all the weather
	// report from the differents APIs
	public void display(int nb_days, boolean h, boolean w, boolean f) {
		String[] data = new String[weathers.size()];
		int i = 0;
		int nb_infos = 1;
		int maxlen = 1;

		// Getting the max length of the suppliers name to create the column
		// with the right width
		for (Weather weather : weathers) {
			if (weather.supplier.length() > maxlen) {
				maxlen = weather.supplier.length();
			}
			data[i] = weather.printWeather(h, w, f); // Getting the reports in a
														// String tab
			i++;
		}

		if (h && w) {
			nb_infos = 3;
		}
		if (h && !w || !h && w) {
			nb_infos = 2;
		}

		// Creating the Table
		Table table = new Table(data, nb_days, nb_infos, maxlen);

		// Displaying the table
		table.display();
	}
}
