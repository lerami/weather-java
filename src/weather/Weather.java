package weather;

public class Weather {
	protected String city;
	protected String supplier;
	protected int nb_days;
	private int[] temp;
	private int[] hum;
	private int[] wind;
	private int[] cond;

	public Weather(String city, int nb_days, String supplier, int[] temp, int[] hum, int[] wind, int[] cond) {
		this.city = city;
		this.nb_days = nb_days;
		this.supplier = supplier;
		this.temp = temp;
		this.hum = hum;
		this.wind = wind;
		this.cond = cond;
	}

	// This function creates a String of the weather information with a
	// particular template so that it will be parsed simply to fill the cells of
	// the table.
	protected String printWeather(boolean h, boolean w, boolean f) {
		if (f) {
			for (int i = 0; i < temp.length; i++) {
				temp[i] = (int) ((double) (temp[i]) * (9 / 5) + 32);
			}
		}

		String weather_data = "=";

		weather_data = supplier + "=";

		if (!h && !w) {
			for (int i = 0; i < nb_days - 1; i++) {
				weather_data = weather_data + cond[i] + "_" + temp[i] + "°=";
			}
			weather_data = weather_data + cond[nb_days - 1] + "_" + temp[nb_days - 1] + "°";
		}

		if (h && w) {
			for (int i = 0; i < nb_days - 1; i++) {
				weather_data = weather_data + cond[i] + "_" + temp[i] + "°_" + hum[i] + "%_" + wind[i] + "mph=";
			}
			weather_data = weather_data + cond[nb_days - 1] + "_" + temp[nb_days - 1] + "°_" + hum[nb_days - 1] + "%_"
					+ wind[nb_days - 1] + "mph";
		}

		if (h && !w) {
			for (int i = 0; i < nb_days - 1; i++) {
				weather_data = weather_data + cond[i] + "_" + temp[i] + "°_" + hum[i] + "%=";
			}
			weather_data = weather_data + cond[nb_days - 1] + "_" + temp[nb_days - 1] + "°_" + hum[nb_days - 1] + "%";
		}
		if (w && !h) {
			for (int i = 0; i < nb_days - 1; i++) {
				weather_data = weather_data + cond[i] + "_" + temp[i] + "°_" + wind[i] + "mph=";
			}
			weather_data = weather_data + cond[nb_days - 1] + "_" + temp[nb_days - 1] + "°_" + wind[nb_days - 1]
					+ "mph";
		}

		return weather_data;

	}

}
