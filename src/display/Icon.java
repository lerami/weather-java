package display;

public class Icon {
	protected String[] content;

	protected Icon(int state) {
		if (state == -1) {
			this.drawNone();
		}
		if (state == 0) {
			this.drawSun();
		}
		if (state == 1) {
			this.drawCloud();
		}
		if (state == 2) {
			this.drawRain();
		}
		if (state == 3) {
			this.drawThunder();
		}
		if (state == 4) {
			this.drawLight();
		}
	}

	private void drawNone() {
		content = new String[6];
		for (int i = 0; i < 6; i++) {
			content[i] = "             ";
		}
		content[2] = "      --     ";
	}

	private void drawSun() {
		content = new String[6];
		content[0] = "             ";
		content[1] = "     \\ /     ";
		content[2] = "     .-.     ";
		content[3] = "  - (   ) -  ";
		content[4] = "     / \\     ";
		content[5] = "             ";
	}

	private void drawCloud() {
		content = new String[6];
		content[0] = "             ";
		content[1] = "     .-.     ";
		content[2] = "    (   )    ";
		content[3] = "   (__(__)   ";
		content[4] = "             ";
		content[5] = "             ";
	}

	private void drawRain() {
		content = new String[6];
		content[0] = "             ";
		content[1] = "     .-.     ";
		content[2] = "    (   )    ";
		content[3] = "   (__(__)   ";
		content[4] = "    ` ` `    ";
		content[5] = "     ` ` `   ";
	}

	private void drawThunder() {
		content = new String[6];
		content[0] = "     .-.     ";
		content[1] = "    (   )    ";
		content[2] = "   (__(__)   ";
		content[3] = "     /_/     ";
		content[4] = "      /_/    ";
		content[5] = "       /     ";
	}

	private void drawLight() {
		content = new String[6];
		content[0] = "             ";
		content[1] = "  _`/\"\" .-.  ";
		content[2] = "   ,\\_ (   ) ";
		content[3] = "      (__(__)";
		content[4] = "             ";
		content[5] = "             ";
	}

}
