package display;

import java.util.regex.Pattern;

public class Line {
	private Cell[] cells;
	protected String[] content;
	private int height;

	// Constructor for the first line (containing the days)
	protected Line(int nb_days, int maxlen_supplier) {
		cells = new Cell[nb_days + 1];
		height = 3;
		this.initializeFirstLine(nb_days, maxlen_supplier);
		this.initializeFirstLineContent(nb_days);
	}

	// Constructor for the other lines containing weather info
	protected Line(String weather_data, int nb_days, int maxlen_supplier, int nb_infos) {
		cells = new Cell[nb_days + 1];
		height = 7;
		this.initializeCells(weather_data, maxlen_supplier, nb_infos);
		this.initializeContent(nb_infos);
	}

	private void initializeFirstLine(int nb_days, int maxlen) {
		for (int i = 0; i < nb_days + 1; i++) {
			this.cells[i] = new Cell(i - 1, maxlen);
		}
	}

	// Creating cells by using the Pattern object to split the long String given
	// by the weather report
	private void initializeCells(String weather_data, int maxlen_supplier, int nb_infos) {
		Pattern p = Pattern.compile("=");
		String[] datacell = p.split(weather_data);
		this.cells[0] = new Cell(datacell[0], maxlen_supplier, nb_infos);
		for (int i = 1; i < datacell.length; i++) {
			this.cells[i] = new Cell(datacell[i], nb_infos);
		}
	}

	// Filling in the first Line
	private void initializeFirstLineContent(int nb_days) {
		content = new String[height];

		for (int i = 0; i < height; i++) {
			content[i] = "";
			for (int j = 0; j < cells.length; j++) {
				content[i] = content[i] + cells[j].content[i];
			}
		}

	}

	// Filling in Line
	private void initializeContent(int nb_infos) {
		content = new String[height];
		for (int i = 0; i < height; i++) {
			content[i] = "";
			for (int j = 0; j < cells.length; j++) {
				content[i] = content[i] + cells[j].content[i];
			}
		}

	}

	// Displaying a Line
	protected void display() {
		for (int i = 0; i < content.length; i++) {
			System.out.println(content[i]);
		}
	}
}
