package display;

public class Table {
	private Line[] lines;

	public Table(String[] weathers, int nb_days, int nb_infos, int maxlen) {
		this.initializeLine(weathers, nb_days, nb_infos, maxlen);
	}

	// Creating Line Object that will constitue the final Table
	private void initializeLine(String[] weathers, int nb_days, int nb_infos, int maxlen) {
		lines = new Line[weathers.length + 1];
		lines[0] = new Line(nb_days, maxlen);
		for (int i = 1; i < weathers.length + 1; i++) {
			lines[i] = new Line(weathers[i - 1], nb_days, maxlen, nb_infos);
		}
	}

	// Displaying the content of each lines so that the entier table is
	// displayed
	public void display() {
		System.out.println("              _______________________________________");
		System.out.println("                                              ");
		System.out.println("                      ~ Your Weather report ~        ");
		System.out.println("                        by Elise and Michel          ");
		System.out.println("              _______________________________________");
		for (int i = 0; i < lines.length; i++) {
			lines[i].display();
		}
	}

}
