package display;

import java.util.regex.Pattern;

public class Cell {
	private int width;
	private int height;
	protected String[] content;

	// Create the cells of the first line (J+0, J+1 ...)
	protected Cell(int noday, int maxlen) {
		width = 6;
		this.initializeDayContent(noday, maxlen);
	}

	// Create the cell containing the weather (conditions, temp, ...)
	protected Cell(String weather_data, int nb_infos) {
		width = 6;
		height = 7;
		this.initializeContent(weather_data);
	}

	// Create the cells for the column containing the suppliers
	protected Cell(String supplier, int width, int nb_infos) {
		this.height = 7;
		this.width = width;
		this.initializeSupplier(supplier);
	}

	// Initialize the content of the cells containing the days
	private void initializeDayContent(int noday, int maxlen) {
		content = new String[3];
		if (noday == -1) {
			content[0] = "+-";
			for (int i = 0; i < maxlen; i++) {
				content[0] = content[0] + "-";
			}
			content[0] = content[0] + "-+";

			content[1] = "| ";
			for (int i = 0; i < maxlen; i++) {
				content[1] = content[1] + " ";
			}
			content[1] = content[1] + " |";

			content[2] = "+-";
			for (int i = 0; i < maxlen; i++) {
				content[2] = content[2] + "-";
			}
			content[2] = content[2] + "-+";
		} else {
			content[0] = "--------------------+";
			content[1] = "        J+" + noday + "         |";
			content[2] = "--------------------+";
		}
	}

	// Initialize the content for the cells containing the weather
	private void initializeContent(String weather_data) {
		content = new String[height];

		Pattern p = Pattern.compile("_");
		String[] data = p.split(weather_data);

		Icon icon = new Icon(Integer.parseInt(data[0]));

		for (int i = 0; i < height - 1; i++) {
			content[i] = icon.content[i] + "       |";
		}

		for (int i = 1; i < data.length; i++) {
			if (data[i].length() < this.width) {
				content[i] = icon.content[i] + " " + data[i];
				for (int j = 0; j < this.width - data[i].length(); j++) {
					content[i] = content[i] + " ";
				}
				content[i] = content[i] + "|";
			} else {
				content[i] = icon.content[i] + " " + data[i] + "|";
			}
		}

		content[height - 1] = "--------------------+";

	}

	// Initialize the content of the cells containing suppliers' name
	private void initializeSupplier(String supplier) {
		content = new String[height];

		if (supplier.length() < this.width) {
			content[0] = "| " + supplier;
			for (int i = 0; i < this.width - supplier.length(); i++) {
				content[0] = content[0] + " ";
			}
			content[0] = content[0] + " |";
		} else {
			content[0] = "| " + supplier + " |";
		}

		for (int i = 1; i < this.height - 1; i++) {
			content[i] = "| ";
			for (int j = 0; j < this.width; j++) {
				content[i] = content[i] + " ";
			}
			content[i] = content[i] + " |";
		}

		content[height - 1] = "+-";
		for (int i = 0; i < this.width; i++) {
			content[height - 1] = content[height - 1] + "-";
		}
		content[height - 1] = content[height - 1] + "-+";

	}

}
