package suppliers;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;

import org.json.*;

import weather.*;

public class APIMetaWeather implements InterfaceMeteo {
	private String name;
	private String url;
	private String USER_AGENT;

	public APIMetaWeather() {
		this.name = "MetaWeather";
		this.url = "https://www.metaweather.com/api/";
		this.USER_AGENT = "Mozilla/5.0";
	}

	// request return a json file
	public Weather weatherReport(String city, int nb_days) throws Exception {

		String url = this.url + "/location/search/?query=" + city;

		HttpURLConnection con = null;
		try {
			URL obj = new URL(url);
			con = (HttpURLConnection) obj.openConnection();
		} catch (MalformedURLException e) {
			System.err.println(e.getMessage());
		}

		// optional default is GET
		con.setRequestMethod("GET");

		// add request header
		con.setRequestProperty("User-Agent", USER_AGENT);

		// Preparing saving of each request by getting the date and the time of
		// the day
		Calendar calendar = Calendar.getInstance();
		int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
		int month = calendar.get(Calendar.MONTH);
		int year = calendar.get(Calendar.YEAR);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);
		int second = calendar.get(Calendar.SECOND);

		String currentDate = String.format("%02d-%02d-%04d", dayOfMonth, month, year);
		String currentTime = String.format("%02d:%02d:%02d", hour, minute, second);
		System.out.print("\n" + currentDate + " " + currentTime);

		int responseCode = con.getResponseCode();
		if (responseCode != HttpURLConnection.HTTP_OK) {
			System.out.println("\t[Response Code: " + responseCode + " ERROR]");

		} else {
			System.out.println("\t[Response Code: " + responseCode + " OK]");
		}

		System.out.println(url);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		String answer = response.toString();
		answer = answer.replace("[", "");
		answer = answer.replace("]", "");

		JSONObject jsonobj = new JSONObject(answer);

		String woeid = jsonobj.getString("woeid");

		// We redo the get request with the woeid, because the request with the
		// city name
		// do not give the temp neither the hum, but the woeid do

		String newurl = this.url + "location/" + woeid;

		HttpURLConnection newcon = null;
		try {
			URL newobj = new URL(newurl);
			newcon = (HttpURLConnection) newobj.openConnection();
		} catch (MalformedURLException e) {
			System.err.println(e.getMessage());
		}

		// optional default is GET
		newcon.setRequestMethod("GET");

		// add request header
		newcon.setRequestProperty("User-Agent", USER_AGENT);

		String currentDate2 = String.format("%02d-%02d-%04d", dayOfMonth, month, year);
		String currentTime2 = String.format("%02d:%02d:%02d", hour, minute, second);
		System.out.print("\n" + currentDate2 + " " + currentTime2);

		int newResponseCode = newcon.getResponseCode();
		if (newResponseCode != HttpURLConnection.HTTP_OK) {
			System.out.println("\t[Response Code: " + newResponseCode + " ERROR]");

		} else {
			System.out.println("\t[Response Code: " + newResponseCode + " OK]");
		}

		System.out.println(newurl);

		BufferedReader newin = new BufferedReader(new InputStreamReader(newcon.getInputStream()));
		String newinputLine;
		StringBuffer newresponse = new StringBuffer();

		while ((newinputLine = newin.readLine()) != null) {
			newresponse.append(newinputLine);
		}
		newin.close();
		String newanswer = newresponse.toString();

		int temp[] = new int[nb_days];
		int hum[] = new int[nb_days];
		int wind[] = new int[nb_days];
		int cond[] = new int[nb_days];

		JSONObject json = new JSONObject(newanswer);
		JSONArray consolidated_weather = json.getJSONArray("consolidated_weather");
		for (int i = 0; i < nb_days; i++) {
			JSONObject child_weather = consolidated_weather.getJSONObject(i);
			temp[i] = child_weather.getInt("the_temp");
			hum[i] = child_weather.getInt("humidity");
			wind[i] = child_weather.getInt("wind_speed");
			String condition = child_weather.getString("weather_state_name");
			cond[i] = -1;
			if (condition.equals("Light Cloud")) {
				cond[i] = LIGHT;
			}
			if (condition.equals("Heavy Cloud")) {
				cond[i] = CLOUD;
			}
			if (condition.contains("Rain") || condition.contains("Showers")) {
				cond[i] = RAIN;
			}
			if (condition.equals("Clear")) {
				cond[i] = SUN;
			}
			if (condition.equals("Thunderstorm")) {
				cond[i] = THUNDER;
			}
		}

		Weather weather = new Weather(city, nb_days, this.name, temp, hum, wind, cond);
		return weather;
	}
}
