package suppliers;

import weather.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
//import java.util.Calendar;

import java.util.Calendar;

import org.json.*;

public class APIYahooWeather implements InterfaceMeteo {
	private String name;
	private String url;
	private String USER_AGENT;

	public APIYahooWeather() {
		this.name = "YahooWeather";
		this.url = "https://query.yahooapis.com/v1/public/yql?q=select%20";
		this.USER_AGENT = "Mozilla/5.0";
	}

	@Override
	public Weather weatherReport(String city, int nb_days) throws Exception {

		String infoGlobal = "*";/*
								 * slctLocation+"%2C%20" +slctHumidity+"%2C%20"
								 * +slctDate+"%2C%20" +slctTypeTemp+"%2C%20"
								 * +slctItem+"%2C%20" +slctWind;
								 */

		// Creating the URL we will be doing GET request on
		String url = this.url + infoGlobal + "%20from%20weather.forecast%20where%20"
				+ "woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" + city + "%22)"
				+ "&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

		HttpURLConnection con = null;
		try {
			URL obj = new URL(url);
			con = (HttpURLConnection) obj.openConnection();
		} catch (MalformedURLException e) {
			System.err.println(e.getMessage());
		}

		// optional default is GET
		con.setRequestMethod("GET");

		// add request header
		con.setRequestProperty("User-Agent", USER_AGENT);

		// Getting date and time for saving requests in requetes.log file
		Calendar calendar = Calendar.getInstance();
		int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
		int month = calendar.get(Calendar.MONTH);
		int year = calendar.get(Calendar.YEAR);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);
		int second = calendar.get(Calendar.SECOND);

		String currentDate = String.format("%02d-%02d-%04d", dayOfMonth, month, year);
		String currentTime = String.format("%02d:%02d:%02d", hour, minute, second);
		System.out.print("\n" + currentDate + " " + currentTime);

		// Getting response code for saving requests
		int responseCode = con.getResponseCode();
		if (responseCode != HttpURLConnection.HTTP_OK) {
			System.out.println("\t[Response Code: " + responseCode + " ERROR]");

		} else {
			System.out.println("\t[Response Code: " + responseCode + " OK]");
		}

		System.out.println(url);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		String answer = response.toString(); // This is the String object
												// containing the JSON

		int temp[] = new int[nb_days];
		int hum[] = new int[nb_days];
		int wind[] = new int[nb_days];
		int cond[] = new int[nb_days];

		// Creating JSONObject to get useful information

		JSONObject jsonobj = null, objQuery = null, objResults = null, objChannel = null, objLocation = null,
				objAtmosphere = null, objItem = null, objWind = null, objCondition = null;
		JSONArray arrayForecast = null;
		try {
			jsonobj = new JSONObject(answer);
			objQuery = jsonobj.getJSONObject("query");
			objResults = objQuery.getJSONObject("results");
			objChannel = objResults.getJSONObject("channel");

			objLocation = objChannel.getJSONObject("location");
			objAtmosphere = objChannel.getJSONObject("atmosphere");
			objItem = objChannel.getJSONObject("item");
			objWind = objChannel.getJSONObject("wind");

			objCondition = objItem.getJSONObject("condition");

			arrayForecast = objItem.getJSONArray("forecast");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		String strLocation = objLocation.getString("city");
		String strAtmosphere = objAtmosphere.getString("humidity");
		String strTemperature = objCondition.getString("temp");
		String strSpeedWind = objWind.getString("speed");

		// Conversion °C to °F:
		// 10°C = 50°F
		temp[0] = (int) (Double.parseDouble(strTemperature) * 0.2);
		hum[0] = Integer.parseInt(strAtmosphere);
		wind[0] = Integer.parseInt(strSpeedWind);

		int i;
		for (i = 0; i < nb_days; i++) {
			if (i == 0) {

			} else {
				JSONObject objDay = arrayForecast.getJSONObject(i);
				String strDayHigh = objDay.getString("high");
				String strDayLow = objDay.getString("low");
				String condition = objDay.getString("text");
				int tempMax = Integer.parseInt(strDayHigh);
				int tempMin = Integer.parseInt(strDayLow);
				int meanTemp = (tempMax + tempMin) / 2;
				cond[i] = -1;
				if (condition.contains("Showers") || condition.contains("Rain")) {
					cond[i] = RAIN;
				}
				if (condition.contains("Thunderstorms")) {
					cond[i] = THUNDER;
				}
				if (condition.contains("Sun")) {
					cond[i] = SUN;
				}
				if (condition.contains("Partly Cloudy")) {
					cond[i] = LIGHT;
				}
				if (condition.contains("Cloud") && !condition.contains("Partly")) {
					cond[i] = CLOUD;
				}
				// Conversion °C to °F:
				// 10°C = 50°F
				temp[i] = (int) ((double) (meanTemp) * 0.2);
			}
		}

		// Finally creating Weather object with all the information for the
		// report
		Weather weather = new Weather(strLocation, nb_days, this.name, temp, hum, wind, cond);

		return weather;
	}

}
