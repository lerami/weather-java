package suppliers;

import weather.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;

import org.json.*;

public class APIPrevisionMeteoCh implements InterfaceMeteo {
	private String name;
	private String url;
	private String USER_AGENT;

	public APIPrevisionMeteoCh() {
		name = "P-Meteo";
		url = "https://www.prevision-meteo.ch/services/json/";
		USER_AGENT = "Mozilla/5.0";
	}

	// request return a json file
	@Override
	public Weather weatherReport(String city, int nb_days) throws Exception {

		String url = this.url + city;

		HttpURLConnection con = null;
		try {
			URL obj = new URL(url);
			con = (HttpURLConnection) obj.openConnection();
		} catch (MalformedURLException e) {
			// System.err.println(e.getMessage());
			System.err.println("Error while fetching weather. Check the arguments.");
		}

		// optional default is GET
		con.setRequestMethod("GET");

		// add request header
		con.setRequestProperty("User-Agent", USER_AGENT);

		// Getting current date and time for saving request
		Calendar calendar = Calendar.getInstance();
		int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
		int month = calendar.get(Calendar.MONTH);
		int year = calendar.get(Calendar.YEAR);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);
		int second = calendar.get(Calendar.SECOND);

		String currentDate = String.format("%02d-%02d-%04d", dayOfMonth, month, year);
		String currentTime = String.format("%02d:%02d:%02d", hour, minute, second);
		System.out.print("\n" + currentDate + " " + currentTime);

		// Getting response code and URL for saving the request in requetes.log
		int responseCode = con.getResponseCode();
		if (responseCode != HttpURLConnection.HTTP_OK) {
			System.out.println("\t[Response Code: " + responseCode + " ERROR]");

		} else {
			System.out.println("\t[Response Code: " + responseCode + " OK]");
		}

		System.out.println(url);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		String answer = response.toString();

		// Preparing to retrieve useful informations from the JSON result of the
		// API
		String H = "12H00";

		int temp[] = new int[nb_days];
		int hum[] = new int[nb_days];
		int wind[] = new int[nb_days];
		int cond[] = new int[nb_days];

		try {
			JSONObject jsonobj = new JSONObject(answer);
			jsonobj.getJSONObject("fcst_day_0");
		} catch (Exception e) {
			System.err.println("City unknown. Try an other one.");
		}

		// Loop to get temperature, humidity, wind speed and conditions for each
		// day
		for (int i = 0; i < nb_days; i++) {
			String fcst_day = "fcst_day_" + i;
			JSONObject jsonobj = new JSONObject(answer);
			JSONObject day = jsonobj.getJSONObject(fcst_day);
			JSONObject hourly_data = day.getJSONObject("hourly_data");
			JSONObject infos = hourly_data.getJSONObject(H);
			temp[i] = infos.getInt("TMP2m");
			hum[i] = infos.getInt("RH2m");
			wind[i] = infos.getInt("WNDSPD10m");
			String condition = day.getString("condition");
			cond[i] = -1;
			if (condition.equals("Ensoleillé")) {
				cond[i] = SUN;
			}
			if (condition.contains("nuageux") && !condition.contains("Faiblement")) {
				cond[i] = CLOUD;
			}
			if (condition.contains("voilé") || condition.equals("Faiblement nuageux")
					|| condition.contains("Eclaircies")) {
				cond[i] = LIGHT;
			}
			if (condition.contains("orage") || condition.contains("Orage")) {
				cond[i] = THUNDER;
			}
			if (condition.contains("Pluie") || condition.contains("Averses")) {
				cond[i] = RAIN;
			}
		}

		// Finally, we can create a Weather object that contains all the
		// information for our report
		Weather weather = new Weather(city, nb_days, this.name, temp, hum, wind, cond);
		return weather;

	}
}